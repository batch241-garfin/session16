

// Arithmetic Operators //

	let x = 4243;
	let y = 2334;

	// Addition Operator

	let sum = x + y;
	console.log("Result of addition operator: "+ sum);

	// Subtraction Operator

	let difference = x - y;
	console.log("Result of the subtraction operator: "+ difference);

	// Multiplication Operator

	let product = x * y;
	console.log("Result of the multiplication operator: "+ product);

	// Division Operator

	let quotient = x / y;
	console.log("Result of division operator: "+ quotient);

	// Modulo Operator

	let remainder = x % y;
	console.log("Result of modulo operator: "+ remainder);

// Assignment Operators //
	// -It adds the value of the right operand to a variable
	// and assigns the result to the variable.

	// (=) Equal Sign

	let assignmentNumber = 8;

	// (+=) Addition Assignment Operator

	assignmentNumber += 2; // property = property + 2;
	console.log("Result of the addition assignment operator: "+ assignmentNumber);

	// (-=) Subtraction Assignment Operator

	assignmentNumber -= 2; // property = property - 2;
	console.log("Result of subtraction assign operator: "+ assignmentNumber);

	// (*=) Multiplication Assignment Operator

	assignmentNumber *= 2; // property = property * 2;
	console.log("Result of multiplication assign operator: "+ assignmentNumber);

	// (/=) Division Assignment Operator

	assignmentNumber /= 2; // property = property / 2;
	console.log("Result of division assign operator: "+ assignmentNumber);

// Multiple Operators and Paranthesis

	// When multiple operators are applied in a single statement
	// it follows the PEMDAS rule (Parenthesis, Exponents,
	// Multi, Divi, Addi, Subtacti).

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: "+ mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: "+ pemdas);

// Increment and Decrement

	// Operators that add or subtract values by 1 and reassigns
	// the value of the variable where the increment or decrement
	// was applied to.

	// (++) Increment

	let z = 1;

		// Pre-increment

		let preIncrement = ++z;
		console.log("Pre- Increment: "+ preIncrement);
		console.log("Z: "+ z);

		// Post-increment

		let postIncrement = z++;
		console.log("Post- Increment: "+ postIncrement);
		console.log("Z: "+ z);

	// (--) Decrement

	let a = 2;

		// Pre-decrement

		let preDecrement = --a;
		console.log("Pre- Decrement: "+ preDecrement);
		console.log("A: "+ a);

		// Post-decrement

		let postDecrement = a--;
		console.log("Post- Decrement: "+ postDecrement);
		console.log("A: "+ a);

// Type Coercion

	// Is the automatic or implicit conversion
	// of values from one data type to another.
	
	let numA = '10';
	let numB = 12;

	// Addition Coercion
	let coercion = numA + numB;

	console.log(coercion); //1012
	console.log(typeof coercion); // String

	// Subtraction Coercion
	let coercion1 = numA - numB;

	console.log(coercion1); //-2
	console.log(typeof coercion);

	// No Coercion
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;

	console.log(""+ nonCoercion); //30
	console.log(""+ typeof nonCoercion);

	// Addition of Number and Boolean

		// true with a value of 1
		// false with the value of 0

	let numE = true + 1;

	console.log(""+ numE); //2
	console.log(""+ typeof numE);

	let numF = false + 1;

	console.log(""+ numF); //1
	console.log(""+ typeof numF);

// Comparison Operator

	let juan = 'juan';

	// (==) Equality Operator

		// Checks whether operands are equal/ have the same
		// content. It also attempts to CONVERT and COMPARE
		// operands of different data types.
		// Returns a boolean value (true/false).

	console.log(1 == 1); //T
	console.log(1 == 2); //F
	console.log(1 == '1'); //T
	console.log(1 == true); //T
	console.log('true' == true); //F

	console.log('juan' == 'juan'); //T
	console.log(juan == 'juan'); //T

	// Inequality Operator

		// Checks whether operands are not equal/ have the
		// same content. It also attempts to CONVERT and
		// COMPARE operands of different data types.
		// Returns a boolean value (true/false).

	console.log(1 != 1); //F
	console.log(1 != 2); //T
	console.log(1 != '1'); //F
	console.log(1 != true); //F
	console.log('true' != true); //T

	console.log('juan' != 'juan'); //F
	console.log(juan != 'juan'); //F

	// Strict Equality Operator

		// Checks whether the operands are equal / have
		// the same content or value. It also COMPARES the
		// data types of two values.

	console.log(1 === 1); //T
	console.log(1 === 2); //F
	console.log(1 === '1'); //F
	console.log(1 === true); //F
	console.log('true' === true); //F
	console.log('juan' === 'juan'); //T
	console.log(juan === 'juan'); //T

	// Strict Inequality Operator

		// Checks whether the operands are not equal / have
		// the same content or value. It also COMPARES the
		// data types of two values.

	console.log(1 !== 1); //F
	console.log(1 !== 2); //T
	console.log(1 !== '1'); //T
	console.log(1 !== true); //T
	console.log('true' !== true); //T
	console.log('juan' !== 'juan'); //F
	console.log(juan !== 'juan'); //F

// Relational Operator

let j = 50;
let k = 65;

	// (>) Greater Than Operator

	let isGreaterThan = j > k;
	console.log(isGreaterThan); //F

	// (<) Less Than Operator

	let isLessThan = j < k;
	console.log(isLessThan); //T

	//(>=) Greater than or Equal Operator

	let isGTorEqual = j >= k;
	console.log(isGTorEqual); //F

	// (<=) Less Than or Equal Operator

	let isLTorEqual = j <= k;
	console.log(isLTorEqual); //T

	// String Equal Operator

	let numStr = "30";
	console.log(j > numStr); //T

	let stringStr = "sample";
	console.log(j > stringStr); //F

// Logical Operator

let isLegalAge = true;
let isRegistered = false;

	// (&& - Double Ampersand) Logical AND Operator

		// It will returns true if all operands are true.
		// true && true = true.
		// true && false = false.

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log(allRequirementsMet); //F

	// (|| - Double Pipe) Logical OR Operator

		// It will returns true if one of the operands are
		// true.
		// true || true = true.
		// true || false = true.
		// false || false = false.

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log(someRequirementsMet); //T

	// (! - Exclamation Point) Logical NOT Operator

		// It will returns the opposite value of the operands.
	let someRequirementsNotMet = !isRegistered;
	console.log(someRequirementsNotMet); //T
